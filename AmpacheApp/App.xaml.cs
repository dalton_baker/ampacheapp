﻿using Xamarin.Forms;
using AmpacheApiWrapper;
using AmpacheApp.ViewModels;
using Nancy.TinyIoc;
using AmpacheApp.Models;
using AmpacheApiWrapper.DataTypes.Authentication;
using Xamarin.Essentials;
using AmpacheApp.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using AmpacheApiWrapper.DataTypes;
using System.Threading.Tasks;

namespace AmpacheApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            AmpacheAuthInfo testUserOnDrdoomsalot = new AmpacheAuthInfo
            {
                AmpacheUrl = "https://drdoomsalot-music.com",
                Password = "test",
                UserName = "test",
            };

            TinyIoCContainer.Current.Register<IAmpacheAuthInfo>(testUserOnDrdoomsalot);
            TinyIoCContainer.Current.Register<IAmpacheApi, AmpacheApi>().AsMultiInstance();
            TinyIoCContainer.Current.Register<IPlaylist, Playlist>();
            TinyIoCContainer.Current.Register<IAmpacheServerData, AmpacheServerData>().AsSingleton();
            TinyIoCContainer.Current.Register<IMusicPlayer, MusicPlayer>().AsSingleton();
            TinyIoCContainer.Current.Register<IAlbumViewModel, AlbumViewModel>().AsMultiInstance();
            TinyIoCContainer.Current.Register<IArtistListViewModel, ArtistListViewModel>().AsMultiInstance();
            TinyIoCContainer.Current.Register<IMusicPlayerViewModel, MusicPlayerViewModel>().AsMultiInstance();
            TinyIoCContainer.Current.Register<IArtistViewModel, ArtistViewModel>().AsMultiInstance();

            InitializeServer();

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void InitializeServer()
        {
            IAmpacheServerData ampacheServer = TinyIoCContainer.Current.Resolve<IAmpacheServerData>();

            string artistListJson = Preferences.Get(Constants.ArtistListPreference, Constants.AmpacheServerDefaultValue);

            if (artistListJson != Constants.AmpacheServerDefaultValue)
            {
                List<Artist> artistList = JsonConvert.DeserializeObject<List<Artist>>(artistListJson);
                ampacheServer.InsertArtists(artistList);
            }

            string albumListJson = Preferences.Get(Constants.AlbumListPreference, Constants.AmpacheServerDefaultValue);

            if (albumListJson != Constants.AmpacheServerDefaultValue)
            {
                List<Album> albumList = JsonConvert.DeserializeObject<List<Album>>(albumListJson);
                ampacheServer.InsertAlbums(albumList);
            }

            string songListJson = Preferences.Get(Constants.SongListPreference, Constants.AmpacheServerDefaultValue);

            if (songListJson != Constants.AmpacheServerDefaultValue)
            {
                List<Song> songList = JsonConvert.DeserializeObject<List<Song>>(songListJson);
                ampacheServer.InsertSongs(songList);
            }
        }
    }
}
