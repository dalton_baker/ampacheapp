﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using AmpacheApp.Views;
using AmpacheApiWrapper.DataTypes;
using AmpacheApiWrapper;
using AmpacheApp.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Essentials;
using System.Linq;
using AmpacheApp.Utilities;

namespace AmpacheApp.ViewModels
{
    public interface IArtistListViewModel
    {
        ObservableCollection<Artist> Artists { get; }
        Command LoadArtistsCommand { get; }
        Command<Artist> ArtistTapped { get; }
        Artist SelectedArtist { get; set; }

        void OnAppearing();
        void OnReAppearing();
    }


    public class ArtistListViewModel : BaseViewModel, IArtistListViewModel
    {
        private Artist _selectedArtist;
        private IAmpacheApi _ampacheApi;
        private readonly IAmpacheServerData _ampacheServerData;
        private bool _isInitalised = false;

        public ObservableCollection<Artist> Artists { get; }
        public Command LoadArtistsCommand { get; }
        public Command<Artist> ArtistTapped { get; }

        public Artist SelectedArtist
        {
            get => _selectedArtist;
            set
            {
                SetProperty(ref _selectedArtist, value);
                OnArtistSelected(value);
            }
        }

        public ArtistListViewModel(IAmpacheApi ampacheApi, IAmpacheServerData ampacheServerData)
        {
            _ampacheApi = ampacheApi;
            _ampacheServerData = ampacheServerData;
            Title = "Browse Artists";
            Artists = new ObservableCollection<Artist>();
            LoadArtistsCommand = new Command(async () => await ExecuteLoadArtistsCommand());
            ArtistTapped = new Command<Artist>(OnArtistSelected);
        }

        private async Task ExecuteLoadArtistsCommand()
        {
            IsBusy = true;

            try
            {
                Artists.Clear();

                if(!_isInitalised)
                {
                    var artistList = _ampacheServerData.GetArtists();

                    if(artistList.Any())
                    {
                        artistList.ForEach(Artists.Add);
                    }
                    else
                    {
                        await DownloadArtists();
                    }

                    _isInitalised = true;
                }
                else
                {
                    await DownloadArtists();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedArtist = null;
        }

        public void OnReAppearing()
        {
            SelectedArtist = null;
        }

        private async Task DownloadArtists()
        {
            List<Artist> artistList = null;

            await Task.Run(() =>
            {
                artistList = _ampacheApi.GetAllArtists();
                _ampacheServerData.InsertArtists(artistList);

                Preferences.Set(Constants.ArtistListPreference, JsonConvert.SerializeObject(artistList));
            });

            artistList.ForEach(Artists.Add);
        }

        private async void OnArtistSelected(Artist artist)
        {
            if (artist == null)
                return;

            _ampacheServerData.CurrentArtist = artist.id;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ArtistPage)}");
            return;
        }
    }
}