﻿using System.ComponentModel;
using Xamarin.Forms;
using AmpacheApp.ViewModels;

namespace AmpacheApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}