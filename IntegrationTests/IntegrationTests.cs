using AmpacheApiWrapper;
using AmpacheApiWrapper.DataTypes;
using AmpacheApiWrapper.DataTypes.Authentication;
using AmpacheApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace IntegrationTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class IntegrationTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void AmpacheServerSerializeDeserialize()
        {
            AmpacheAuthInfo testUserOnDrdoomsalot = new AmpacheAuthInfo
            {
                AmpacheUrl = "https://drdoomsalot-music.com",
                Password = "test",
                UserName = "test",
            };

            var ampacheApi = new AmpacheApi(testUserOnDrdoomsalot);

            List<Artist> artistList = ampacheApi.GetAllArtists();
            List<Album> albumList = new List<Album>();
            List<Song> songList = new List<Song>();
            foreach(var artist in artistList)
            {
                albumList.AddRange(ampacheApi.GetArtistAlbums(artist.id));
            }
            foreach (var album in albumList)
            {
                songList.AddRange(ampacheApi.GetAlbumSongs(album.id));
            }

            string atristsSerialized = JsonConvert.SerializeObject(artistList);
            string albumsSerialized = JsonConvert.SerializeObject(albumList);
            string songsSerialized = JsonConvert.SerializeObject(songList);

            TestContext.WriteLine($"atristsSerialized:\n{atristsSerialized}");
            TestContext.WriteLine($"albumsSerialized:\n{albumsSerialized}");
            TestContext.WriteLine($"songsSerialized:\n{songsSerialized}");

            List<Artist> artistLisDeserialize = JsonConvert.DeserializeObject<List<Artist>>(atristsSerialized);
            List<Album> albumListDeserialize = JsonConvert.DeserializeObject<List<Album>>(albumsSerialized);
            List<Song> songListDeserialize = JsonConvert.DeserializeObject<List<Song>>(songsSerialized);

            Assert.AreEqual(artistList.Count, artistLisDeserialize.Count);
            Assert.AreEqual(albumList.Count, albumListDeserialize.Count);
            Assert.AreEqual(songList.Count, songListDeserialize.Count);
        }
    }
}
