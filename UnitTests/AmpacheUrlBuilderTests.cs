﻿using AmpacheApiWrapper;
using AmpacheApiWrapper.DataTypes;
using AmpacheApiWrapper.DataTypes.Authentication;
using AmpacheApiWrapper.Workers;
using Bogus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AmpacheUrlBuilderTests
    {
        public TestContext TestContext { get; set; }
        private Faker _faker = new Faker();


        [TestMethod]
        public void CreateSutTest()
        {
            //Arrange

            //Act
            AmpacheUrlBuilder urlBuilder = new AmpacheUrlBuilder();

            //Assert
            Assert.IsNotNull(urlBuilder);
        }

        [TestMethod]
        public void CreateSutTest_WithAmpacheAuthInfo()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            string auth = _faker.Internet.Password();
            AmpacheAuthInfo ampacheAuthInfo = new AmpacheAuthInfo
            {
                AmpacheUrl = baseUrl,
                AuthToken = auth
            };

            //Act
            string builtUrl = new AmpacheUrlBuilder(ampacheAuthInfo).Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual($"{baseUrl}/server/json.server.php?auth={auth}", builtUrl);
        }

        [TestMethod]
        public void BuildTest_WithBaseUrl()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            AmpacheUrlBuilder urlBuilder = new AmpacheUrlBuilder().WithBaseUrl(baseUrl);

            //Act
            string builtUrl = urlBuilder.Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual(baseUrl + "/server/json.server.php", builtUrl);
        }

        [TestMethod]
        public void BuildTest_WithBaseUrlAndTrailingSlash()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            AmpacheUrlBuilder urlBuilder = new AmpacheUrlBuilder().WithBaseUrl(baseUrl + "/");

            //Act
            string builtUrl = urlBuilder.Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual(baseUrl + "/server/json.server.php", builtUrl);
        }

        [TestMethod]
        public void BuildTest_NoBaseUrl()
        {
            //Arrange
            AmpacheUrlBuilder urlBuilder = new AmpacheUrlBuilder();

            //Act
            string builtUrl = urlBuilder.Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual(string.Empty, builtUrl);
        }

        [TestMethod]
        public void WithBaseUrlTest()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            AmpacheUrlBuilder urlBuilder = new AmpacheUrlBuilder().WithBaseUrl(baseUrl + "/");

            //Act
            string builtUrl = urlBuilder.Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual(baseUrl + "/server/json.server.php", builtUrl);
        }

        [TestMethod]
        public void WithAuthenticationTest()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            string auth = _faker.Internet.Password();

            //Act
            string builtUrl = new AmpacheUrlBuilder().WithBaseUrl(baseUrl).WithAuthentication(auth).Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual($"{baseUrl}/server/json.server.php?auth={auth}", builtUrl);
        }


        [TestMethod]
        public void WithActionTest()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            string action = _faker.Database.Column();

            //Act
            string builtUrl = new AmpacheUrlBuilder().WithBaseUrl(baseUrl).WithAction(action).Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual($"{baseUrl}/server/json.server.php?action={action}", builtUrl);
        }

        [TestMethod]
        public void WithActionAndAmpacheAuthTest()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            string action = _faker.Database.Column();
            string auth = _faker.Internet.Password();
            AmpacheAuthInfo ampacheAuthInfo = new AmpacheAuthInfo
            {
                AmpacheUrl = baseUrl,
                AuthToken = auth
            };

            //Act
            string builtUrl = new AmpacheUrlBuilder(ampacheAuthInfo).WithAction(action).Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.AreEqual($"{baseUrl}/server/json.server.php?auth={auth}&action={action}", builtUrl);
        }

        [TestMethod]
        public void WithLoginInfoTest()
        {
            //Arrange
            string baseUrl = _faker.Internet.DomainName();
            string userName = _faker.Internet.UserName();
            string password = _faker.Internet.Password();
            AmpacheAuthInfo ampacheAuthInfo = new AmpacheAuthInfo
            {
                AmpacheUrl = baseUrl,
                UserName = userName,
                Password = password,
            };

            //Act
            string builtUrl = new AmpacheUrlBuilder().WithLoginInfo(ampacheAuthInfo).Build();
            TestContext.WriteLine(builtUrl);

            //Assert
            Assert.IsFalse(string.IsNullOrEmpty(builtUrl));
        }
    }
}
